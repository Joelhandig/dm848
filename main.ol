include "SubstringInterface.iol"
include "console.iol"

outputPort Substr
{
    Interfaces: SubstringInterface
}

embedded
{
    Java: "Substring" in Substr
}

main
{
    substr@Substr({ .a = "some text", .b = "text"})(resp);
    print@Console(resp + "\n")()
}
