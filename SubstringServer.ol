include "SubstringInterface.iol"
include "console.iol"

outputPort Substr
{
    Interfaces: SubstringInterface
}

inputPort SubstrService
{
    Location: "socket://localhost:8081"
    Protocol: sodep
    Interfaces: SubstringInterface
}

embedded
{
    Java: "Substring" in Substr
}

execution
{
    concurrent
}

main
{
    
    substr( msg )(resp)
    {
        substr@Substr( { .a = msg.a, .b = msg.b} )(resp)
    }
}
