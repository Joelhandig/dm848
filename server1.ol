include "PrintInterface.iol"

inputPort PrintService
{
    Location: "socket://localhost:8081"
    Protocol: sodep
    Interfaces: PrintInterface
}

execution
{
    concurrent
}


main
{
    print( msg)(response)
    {
        response = "This is from server 1 " + msg + "\n"
    }
}
