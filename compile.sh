#!/bin/bash
mkdir lib
javac -cp $JOLIE_HOME/lib/libjolie.jar:$JOLIE_HOME/jolie.jar Substring.java
jar cvf Substring.jar Substring.class
cp Substring.jar lib/Substring.jar
rm Substring.class
