import jolie.runtime.JavaService;
import jolie.runtime.Value;

public class Substring extends JavaService
{
    public Boolean substr(Value v)
    {
        String a = v.getFirstChild("a").strValue();
        String b = v.getFirstChild("b").strValue();
        
        
        return a.contains(b);
    }
}